## Laravel Framework
---

### Packages
- [danielstjules/Stringy]: A PHP string manipulation library with multibyte support
- [svenluijten/flex-env]: Edit your .env file in Laravel directly from the command line.
- [laravolt/avatar]: Display unique avatar for any user based on their name. Can be used as default avatar when user has not uploaded the avatar image itself.
- [LeShadow/ArtisanExtended]: A heap of commands for an extended artisan experience.


[//]: # (Reference links)
[danielstjules/Stringy]: <https://github.com/danielstjules/Stringy>
[svenluijten/flex-env]: <https://github.com/svenluijten/flex-env>
[laravolt/avatar]: <https://github.com/laravolt/avatar>
[LeShadow/ArtisanExtended]: <https://github.com/LeShadow/ArtisanExtended>